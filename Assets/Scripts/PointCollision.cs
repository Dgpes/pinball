﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointCollision : MonoBehaviour
{

   void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bouncer"){
            PointCounter.points++;
        }
        else if(collision.gameObject.tag == "Disappear"){
            PointCounter.points += 10;
        }
        else if (collision.gameObject.tag == "Lose"){
            Vector3 reset = new Vector3(17.5f, -1.5f, 16f);
            transform.position = reset;
        }
    }
}
