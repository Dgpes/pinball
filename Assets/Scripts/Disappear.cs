﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disappear : MonoBehaviour
{
    // Start is called before the first frame update
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player"){
            Vector3 move = new Vector3(200f, 200f, 200f);
            transform.position = move;
        }
    }
}
